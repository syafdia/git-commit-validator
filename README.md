# Bitbucket Pipelines Pipe:  Git Commit Validator

Validate commit message on your Bitbucket Pipelines.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: docker://syafdia/git-commit-validator:latest
  variables:
    COMMIT_MESSAGES: $COMMIT_MESSAGES
    MESSAGE_FORMAT: $MESSAGE_FORMAT
```

## Variables

| Variable      	| Usage    |
| ------------------|---------|
| COMMIT_MESSAGES(*)| Commit message, separated by newline (\ln)
| MESSAGE_FORMAT | Valid regex for message format |
| TARGET_LANG | Language ID |
(*) = required variable.



## Support

If you'd like help with this pipe, or you have an issue or feature request please email to syafdia@gmail.com

## License

MIT License. Copyright 2021 Syafdia Okta.