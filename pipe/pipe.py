import os
import re
import sys
from typing import List

from bitbucket_pipes_toolkit import Pipe, yaml
from langdetect import detect_langs, DetectorFactory, language

MIN_PROB = 0.5

DEFAULT_TARGET_LANG = 'en'
DEFAULT_MESSAGE_FORMAT = '\[XX-\d+][\w\d\s]+'
class InvalidParameter(Exception):
  pass

def is_valid_format(msg: str, msg_fmt: str) -> bool:
  return re.match(re.compile(msg_fmt), msg) != None

def is_valid_lang(msg: str, target_lang: str, min_prob: float) -> bool:
  langs = detect_langs(msg)
  for l in langs:
    if l.lang == target_lang:
      return l.prob > min_prob

  return False

def validate(commit_msgs: List[str], msg_fmt: str, target_lang: str):
  for commit_msg in commit_msgs:
    if commit_msg == '':
      continue

    pipe.log_info(f"Got commit message `{commit_msg}`")

    if is_valid_format(commit_msg, msg_fmt) == False:
      raise InvalidParameter(f"Message format for `{commit_msg}` is not valid, please use {msg_fmt}")

    if is_valid_lang(commit_msg, target_lang, MIN_PROB) == False:
      raise InvalidParameter(f"Language for `{commit_msg}` is not valid, please use {target_lang.upper()}")

class GitCommitValidatorPipe(Pipe):

  def run(self):
    super().run()

    self.log_info('Validating commit message...')

    commit_msgs = self.get_variable('COMMIT_MESSAGES').split('\n')
    msg_fmt = self.get_variable('MESSAGE_FORMAT')
    target_lang = self.get_variable('TARGET_LANG')

    try:
      validate(commit_msgs, msg_fmt, target_lang)
    except Exception as e:
      self.log_info(e)
      sys.exit(1)

    self.log_info('OK')

if __name__ == '__main__':
  schema = {
    'COMMIT_MESSAGES': {'type': 'string', 'required': True},
    'MESSAGE_FORMAT': {'type': 'string', 'required': False, 'default': DEFAULT_MESSAGE_FORMAT},
    'TARGET_LANG': {'type': 'string', 'required': False, 'default': DEFAULT_TARGET_LANG},
  }

  with open('/pipe.yml', 'r') as metadata_file:
    metadata = yaml.safe_load(metadata_file.read())

  pipe = GitCommitValidatorPipe(schema=schema, pipe_metadata=metadata)
  pipe.run()