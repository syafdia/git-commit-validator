import unittest
import sys

from pipe.pipe import is_valid_format, is_valid_lang

class TestIsValidFormat(unittest.TestCase):
  def test_format_is_valid(self):
    self.assertTrue(is_valid_format('[AD-1992] This is the commit message', '\[AD-\d+][\w\d\s]+'))

  def test_format_is_not_valid(self):
    self.assertFalse(is_valid_format('[AD999] This is the commit message', '\[AD-\d+][\w\d\s]+'))

class TestIsValidLang(unittest.TestCase):
  def test_message_is_valid_en(self):
    self.assertTrue(is_valid_lang('[AD-1992] This is the commit message', 'en', 0.5))

  def test_message_is_not_valid_en(self):
    self.assertFalse(is_valid_lang('[AD-1992] Ini adalah pesan commit', 'en', 0.5))

if __name__ == '__main__':
  unittest.main()